/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package NCBIElements;

import java.util.HashMap;

/**
 *
 * @author murloc
 */
public class Node 
{
    private int id;
    private Names n;
    //private Division d;
    private String rank;
    private int parentId;
    private String uri;
    
    public Node(int id, Names n, String rank, int parentId)
    {
        this.id = id;
        this.n = n;
        //this.d = d;
        this.parentId = parentId;
        this.rank = rank.replaceAll(" ", "_");
        this.uri = "-1";
    }
    
    public String getRank()
    {
        return this.rank;
    }
    
    public int getParentId()
    {
        return this.parentId;
    }
    
    /*public int getDivisionId()
    {
        return this.d.getId();
    }*/
    
    public String getLabelsTtl()
    {
        return this.n.getTtlConcat();
    }
    
    private String generateUri(String baseUri){
        String firstLabel = this.n.getFirstPrefLabel();
        String ret = baseUri;
        if(firstLabel != null){
            firstLabel = firstLabel.replaceAll(" ", "_").replaceAll("\\.", "");
            ret += firstLabel;
        }
        else{
            ret += ""+this.id;
        }
        this.uri = ret;
        return ret;
    }
    
    public String getUri(String baseUri){
        String ret = null;
        if(!this.uri.equalsIgnoreCase("-1")){
            ret = this.uri;
        }
        else{
            ret = this.generateUri(baseUri);
        }
        return ret;
    }
    
    public boolean isDescendantOfLimit(int limitId, HashMap<Integer, Node> nodes){
        boolean ret = false;
        if(this.parentId == limitId){
            ret = true;
        }
        else{
            Node nParent = nodes.get(this.parentId);
            if(nParent != null){
                ret = nParent.isDescendantOfLimit(limitId, nodes);
            }
        }
        return ret;
    }
    
}
