/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ncbi2rkb;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author murloc
 */
public class Ncbi2RKB 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        //SparqlProxy spOut = SparqlProxy.getSparqlProxy("http://localhost:3030/Ncbi2RKB_out/");
        
        NCBIExtractor ne = new NCBIExtractor();
        
        /*System.out.println("Loading divisions ...");
       int nbDiv = ne.loadDivision("in/division.dmp");
        System.out.println("Divisions loaded");*/
        
        System.out.println("Loading names ...");
        int nbNames = ne.loadNames("in/names.dmp");
        System.out.println("Names loaded");
        
        System.out.println("Loading nodes...");
        int nbNodes = ne.loadNodes("in/nodes.dmp");
        System.out.println("Nodes loaded");
        
        
        StringBuilder out = new StringBuilder();
        System.out.println("Export ontological module prefixes ... ");
        try {
            String modulePrefixes = FileUtils.readFileToString(new File("in/agronomicTaxon_prefix.ttl"), "utf-8");
            out.append(modulePrefixes);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Prefixes exported");
        
        System.out.println("Export ontological module ... ");
        try {
            String moduleTriples = FileUtils.readFileToString(new File("in/agronomicTaxon_2.ttl"), "utf-8");
            out.append(moduleTriples);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Module exported");
        
        System.out.println("get all triples");
        StringBuilder ret = ne.exportNodes(4564);
        out.append(ret);
        System.out.println("Exported");
        
        
        String dateFileName = new SimpleDateFormat("dd-MM_HH-mm_").format(new Date());
        System.out.println("Exporting RKB to file : out_NCBI.ttl");
       try {
            FileUtils.write(new File("out_NCBI.ttl"), out);
        } catch (IOException ex) {
            System.err.println("FILE WRITTER ERROR");
        }
        System.out.println("File generated");
        
        System.out.println("NCBI processed!");
        /*System.out.println("--------------------------------");
        
        System.out.println("Nb nodes loaded : "+nbNodes);
        System.out.println("Nb labesl : "+nbNames);*/
        //System.out.println("Nb Division : "+nbDiv);
    }
    
}
